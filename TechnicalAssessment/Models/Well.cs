﻿using System;

namespace TechnicalAssessment.Models
{
    public class Well : BaseModel
    {
        public DateTimeOffset WellCreatedAt { get; set; }
        public DateTimeOffset WellUpdatedAt { get; set; }
        public int PlatformID { get; set; }
        public Platform Platform { get; set; }
    }
}
