﻿using System;
using System.Collections.Generic;

namespace TechnicalAssessment.Models
{
    public class Platform : BaseModel
    {
        public DateTimeOffset? PlatformCreatedAt { get; set; }
        public DateTimeOffset? PlatformUpdatedAt { get; set; }
        public List<Well> Wells { get; set; }
    }
}
