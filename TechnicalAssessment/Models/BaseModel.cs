﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TechnicalAssessment.Models
{
    public class BaseModel
    {
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
    }
}
