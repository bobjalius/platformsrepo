﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnicalAssessment.PlatformWellRepo.Models;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.DTOs
{
    public class PlatformDto
    {
        public PlatformDto()
        {
            Platforms = new List<Models.Platform>();
            PlatformApi = new List<PlatformWellRepo.Models.Platform>();
        }

        public List<Models.Platform> Platforms { get; set; }
        public List<PlatformWellRepo.Models.Platform> PlatformApi { get; set; }
    }
}
