﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.DataAccess;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.Services
{
    public class DataService : IDataService
    {
        private readonly Context _context;

        public DataService(Context context)
        {
            _context = context;
        }

        public async Task<List<Platform>> GetAllPlatformsAsync(CancellationToken cancellationToken = default)
        {
            var result = await Task.Run(() =>
            {
                if (_context.Platforms != null && _context.Platforms.Any())
                {
                    return _context.Platforms
                    .Include(x => x.Wells)
                    .OrderByDescending(x => x.Id)?.ToList();
                }
                return null;
            }, cancellationToken);

            return result;
        }

        public async Task UpsertPlatformAsync(Platform platform, CancellationToken cancellationToken = default)
        {
            var tmp = await _context.Platforms?.FirstOrDefaultAsync(x => x.Id == platform.Id, cancellationToken);

            if (tmp == null)
            {
                await _context.AddAsync(platform, cancellationToken);
            }
            else
            {
                _context.Update(platform);
            }

            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task UpsertPlatformsAsync(List<Platform> platforms, CancellationToken cancellationToken = default)
        {
            var tmp = _context.Platforms?.Where(x => platforms.Select(r => r.Id).Contains(x.Id))
                ?.Include(x => x.Wells).ToList();
            var tmpIn = platforms.Where(x => !tmp.Select(r => r.Id).Contains(x.Id));

            if (tmp != null && tmp.Any())
            {
                tmp.ForEach(x =>
                {
                    CopyPlatformValue(platforms.FirstOrDefault(p => p.Id == x.Id), x, false);
                });

                _context.UpdateRange(tmp.Where(x => platforms.Select(r => r.Id).Contains(x.Id)));
            }

            if (tmpIn != null && tmpIn.Any())
            {
                await _context.AddRangeAsync(tmpIn, cancellationToken);
            }

            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeletePlatformAsync(int id, CancellationToken cancellationToken = default)
        {
            var rule = await _context.Platforms?.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (rule != null)
            {
                _context.Remove(rule);

                await _context.SaveChangesAsync(cancellationToken);
            }
        }

        public void CopyPlatformValue(Platform from, Platform to, bool mapCreateDate = true)
        {
            if (from == null)
                return;

            if (to == null)
                to = new Platform();

            to.Id = from.Id;
            to.UniqueName = from.UniqueName;
            to.Latitude = from.Latitude;
            to.Longitude = from.Longitude;
            to.PlatformCreatedAt = from.PlatformCreatedAt;
            to.PlatformUpdatedAt = from.PlatformUpdatedAt;
            to.UpdatedAt = from.UpdatedAt;
            if (mapCreateDate)
                to.CreatedAt = from.CreatedAt;

            from.Wells?.ForEach(x =>
            {
                CopyWellValue(x, to.Wells?.FirstOrDefault(p => p.Id == x.Id), false);
            });
        }

        public void CopyWellValue(Well from, Well to, bool mapCreateDate = true)
        {
            if (from == null)
                return;

            if (to == null)
                to = new Well();

            to.Id = from.Id;
            to.UniqueName = from.UniqueName;
            to.Latitude = from.Latitude;
            to.Longitude = from.Longitude;
            to.WellCreatedAt = from.WellCreatedAt;
            to.WellUpdatedAt = from.WellUpdatedAt;
            to.PlatformID = from.PlatformID;
            to.UpdatedAt = from.UpdatedAt;
            if (mapCreateDate)
                to.CreatedAt = from.CreatedAt;
        }
    }
}
