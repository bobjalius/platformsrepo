﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.DTOs;
using TechnicalAssessment.Extensions;
using TechnicalAssessment.PlatformWellRepo;

namespace TechnicalAssessment.Services
{
    public class SyncDataService : ISyncDataService
    {
        private readonly IApiRepo _repo;
        private readonly IDataService _data;
        public SyncDataService(IApiRepo repo, IDataService data)
        {
            _repo = repo;
            _data = data;
        }

        public async Task<PlatformDto> SyncData(bool updateDatabase = true, CancellationToken cancellationToken = default)
        {
            //Get data from API
            var apiData = await _repo.GetPlatforms(cancellationToken);

            if (updateDatabase && apiData != null && apiData.Any())
            {
                var dbdata = apiData.Select(x => x.ToDataPlatform()).ToList();

                //to sync api data into db
                await _data.UpsertPlatformsAsync(dbdata);
            }

            var result = new PlatformDto
            {
                PlatformApi = apiData?.OrderBy(x => x.Id).ToList(),
                Platforms = (await _data.GetAllPlatformsAsync(cancellationToken)).OrderBy(x => x.Id).ToList()
            };

            return result;
        }
    }
}
