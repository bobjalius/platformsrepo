﻿using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.DTOs;

namespace TechnicalAssessment.Services
{
    public interface ISyncDataService
    {
        Task<PlatformDto> SyncData(bool updateDatabase = true, CancellationToken cancellationToken = default);
    }
}
