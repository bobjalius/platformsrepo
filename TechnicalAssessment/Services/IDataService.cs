﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.Services
{
    public interface IDataService
    {
        Task<List<Platform>> GetAllPlatformsAsync(CancellationToken cancellationToken = default);
        Task UpsertPlatformAsync(Platform platform, CancellationToken cancellationToken = default);
        Task UpsertPlatformsAsync(List<Platform> rules, CancellationToken cancellationToken = default);
        Task DeletePlatformAsync(int id, CancellationToken cancellationToken = default);
    }
}
