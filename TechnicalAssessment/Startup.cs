using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TechnicalAssessment.DataAccess;
using TechnicalAssessment.PlatformWellRepo;
using TechnicalAssessment.PlatformWellRepo.Auth;
using Microsoft.EntityFrameworkCore;
using TechnicalAssessment.Services;

namespace TechnicalAssessment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddHttpClient();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IDataService, DataService>();
            services.AddScoped<ISyncDataService, SyncDataService>();

            if (Configuration.GetSection("PlatformAPIs").GetValue<bool>("UseDummy"))
                services.AddScoped<IApiRepo, DummyApiRepo>();
            else
                services.AddScoped<IApiRepo, ApiRepo>();

            services.AddDbContext<Context>(options =>
            options.UseSqlServer(
                Configuration.GetConnectionString("DataContext"),
                b => b.MigrationsAssembly(typeof(Context).Assembly.FullName)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });

            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<Context>();
            try
            {
                context.Database?.EnsureCreated();
            }
            catch (System.Exception ex)
            {
                //log error
            }
        }
    }
}
