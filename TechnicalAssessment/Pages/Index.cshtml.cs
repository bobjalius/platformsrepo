﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnicalAssessment.DTOs;
using TechnicalAssessment.PlatformWellRepo;
using TechnicalAssessment.PlatformWellRepo.Auth;
using TechnicalAssessment.Services;

namespace TechnicalAssessment.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IAuthenticationService _auth;
        private readonly IApiRepo _repo;
        private readonly ISyncDataService _syncData;
        private readonly IConfiguration _configuration;

        public IndexModel(ILogger<IndexModel> logger, ISyncDataService syncData, IConfiguration configuration)
        {
            _logger = logger;            
            _syncData = syncData;
            _configuration = configuration;
        }

        public async Task OnGet()
        {
            //Data = await _syncData.SyncData(false);
            await SyncData();
            Source = _configuration.GetSection("PlatformAPIs").GetValue<bool>("UseDummy") ? "Dummy" : "Actual";
        }

        public async Task SyncData()
        {
            Data = await _syncData.SyncData(true);
        }

        public PlatformDto Data { get; set; }
        public string Source { get; set; }
    }
}
