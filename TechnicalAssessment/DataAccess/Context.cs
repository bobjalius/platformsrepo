﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.Models;

namespace TechnicalAssessment.DataAccess
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        { }

        public DbSet<Platform> Platforms { get; set; }
        public DbSet<Well> Wells { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Platform>().HasKey(x => x.Id);
            modelBuilder.Entity<Platform>().Property(x => x.Id).ValueGeneratedNever();

            modelBuilder.Entity<Well>().HasKey(x => x.Id);
            modelBuilder.Entity<Well>().Property(x => x.Id).ValueGeneratedNever();


            modelBuilder.Entity<Platform>().HasMany(x => x.Wells).WithOne(x => x.Platform).HasForeignKey(x => x.PlatformID);
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var changes = from e in this.ChangeTracker.Entries()
                          where e.State != EntityState.Unchanged && e.Entity is BaseModel
                          select e;

            foreach (var change in changes)
            {
                var model = (BaseModel)change.Entity;

                if (change.State == EntityState.Added)
                {
                    model.CreatedAt = model.CreatedAt != null && model.CreatedAt > DateTimeOffset.MinValue ? model.CreatedAt : DateTimeOffset.Now;
                    model.UpdatedAt = model.CreatedAt;
                }
                else if (change.State == EntityState.Modified)
                {
                    model.UpdatedAt = DateTimeOffset.Now;
                }
            }
            return await base.SaveChangesAsync(cancellationToken);
        }

    }
}
