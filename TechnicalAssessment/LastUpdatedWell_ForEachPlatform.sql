﻿
SELECT p.UniqueName 'Platform',
       r.UniqueName 'Last Updated Well',
       r.WellUpdatedAt 'Last Updated Date'
FROM   Platforms p
       CROSS APPLY (SELECT TOP(1) w.UniqueName,
                                 w.WellUpdatedAt,
                                 w.PlatformID
                    FROM   Wells w
                    WHERE  w.PlatformID=p.Id
                    ORDER  BY w.WellUpdatedAt DESC) r
