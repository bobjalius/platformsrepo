﻿using System.Linq;

namespace TechnicalAssessment.Extensions
{
    public static class ModelConverter
    {
        public static Models.Platform ToDataPlatform(this PlatformWellRepo.Models.Platform platform)
        {
            return new Models.Platform
            {
                Id = platform.Id,
                UniqueName = platform.UniqueName,
                Latitude = platform.Latitude,
                Longitude = platform.Longitude,
                PlatformCreatedAt = platform.CreatedAt,
                PlatformUpdatedAt = platform.UpdatedAt,
                Wells = platform.Well?.Select(x => x.ToDataWell()).ToList()
            };
        }

        public static Models.Well ToDataWell(this PlatformWellRepo.Models.Well well)
        {
            return new Models.Well
            {
                Id = well.Id,
                UniqueName = well.UniqueName,
                Latitude = well.Latitude,
                Longitude = well.Longitude,
                WellCreatedAt = well.CreatedAt,
                WellUpdatedAt = well.UpdatedAt,
                PlatformID = well.PlatformID
            };
        }
    }
}
