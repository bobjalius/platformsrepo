﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.PlatformWellRepo.Models;

namespace TechnicalAssessment.PlatformWellRepo
{
    public interface IApiRepo
    {
        Task<List<Platform>> GetPlatforms(CancellationToken cancellationToken = default);
    }
}
