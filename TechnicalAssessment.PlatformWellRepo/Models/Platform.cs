﻿using System.Collections.Generic;

namespace TechnicalAssessment.PlatformWellRepo.Models
{
    public class Platform : BaseModel
    {
        public List<Well> Well { get; set; }
    }
}
