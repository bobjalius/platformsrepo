﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using TechnicalAssessment.PlatformWellRepo.Auth;
using TechnicalAssessment.PlatformWellRepo.Models;

namespace TechnicalAssessment.PlatformWellRepo
{
    public class DummyApiRepo : IApiRepo
    {
        private const string ApiUrl = "http://test-demo.aemenersol.com/api/PlatformWell/GetPlatformWellDummy";

        private readonly IHttpClientFactory _clientFactory;
        private readonly IAuthenticationService _authService;

        public DummyApiRepo(IHttpClientFactory clientFactory, IAuthenticationService authService)
        {
            _clientFactory = clientFactory;
            _authService = authService;
        }

        public async Task<List<Platform>> GetPlatforms(CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, ApiUrl);
            var client = _clientFactory.CreateClient();
            var token = await _authService.GetToken();
            if (!string.IsNullOrWhiteSpace(token))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var response = await client.SendAsync(request, cancellationToken);

            if (!response.IsSuccessStatusCode)
                return new List<Platform>();

            return await response.Content.ReadFromJsonAsync<List<Platform>>();
        }
    }
}
