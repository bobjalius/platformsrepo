﻿using System.Threading;
using System.Threading.Tasks;

namespace TechnicalAssessment.PlatformWellRepo.Auth
{
    public interface IAuthenticationService
    {
        Task<string> GetToken(CancellationToken cancellationToken = default);
    }
}
