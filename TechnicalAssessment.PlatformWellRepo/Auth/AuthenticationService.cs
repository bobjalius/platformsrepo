﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace TechnicalAssessment.PlatformWellRepo.Auth
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string TokenUrl = "http://test-demo.aemenersol.com/api/Account/Login";

        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;

        private string _token;

        public AuthenticationService(IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        public async Task<string> GetToken(CancellationToken cancellationToken = default)
        {
            if (!string.IsNullOrWhiteSpace(_token))
                return _token;

            _token = await GenerateToken(cancellationToken);

            return _token;
        }

        public async Task<string> GenerateToken(CancellationToken cancellationToken = default)
        {
            var username = _configuration.GetValue<string>("USERNAME");
            var password = _configuration.GetValue<string>("PASSWORD");
            var client = _clientFactory.CreateClient();
            var request = new HttpRequestMessage(HttpMethod.Post, TokenUrl)
            {
                Content = JsonContent.Create(new { Username = username, Password = password })
            };
            var response = await client.SendAsync(request, cancellationToken);

            if (!response.IsSuccessStatusCode)
                return null;

            return await response.Content.ReadFromJsonAsync<string>();
        }
    }
}
